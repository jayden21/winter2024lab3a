public class Application {
	public static void main(String []args){
		Student s1 = new Student("Jayden", 18, "badminton", true);
		Student s2 = new Student("Momo", 21, "esport", false);
		s1.printSport();
		
		Student[] section3 = new Student[3];
		section3[0] = s1;
		section3[1] = s2;
		section3[2] = new Student("Elsa", 16, "snowboarding", false);
		System.out.println(section3[2].age);
	}
}