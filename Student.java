	public class Student{
	//Attributes
	String name;
	int age;
	String sport;
	boolean isAthlete;
	
	public Student(String n, int a, String s, boolean athlete){
		name = n;
		age = a;
		sport = s;
		isAthlete = athlete;
	}
	public String printOnlyName(){
		return "Hi! My name is " + name;
	}
	public void printSport(){
		if (isAthlete)
			System.out.println("I am a student-athlete and I'm currently playing " + sport + " for Dawson team");
		else 
			System.out.println("I am not a student-athlete but sometimes I play " + sport + " at home");
	}
	public void sayHi(){
		System.out.println("Hi Java, my name is " + name + " and I'm " + age + " years of age");
	}
}